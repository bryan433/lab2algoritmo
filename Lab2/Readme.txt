Empezando el pograma: Al iniciar el programa nos pedira ingresar el tamaño de la pila, la cual sera el maximo de datos que podamos ingresar, este consistira en 4 opciones, la primera consiste en poder agregar elementos (en este programa solo podremos ingresar numero enteros), la segunda opcion nos dejara remover un dato, el cual solo nos dejara eliminar el ultimo dato ingresado, ya que en eso consiste la definicion de una pila, la tercera opcion nos recorrera la pila y nos la imprimira por pantalla los datos que contiene la pila y la ultima opcion solamente nos cerrara el programa.

Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/lab2algoritmo clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

Sistema operativo:
Debian GNU/Linux 9 (stretch) 64-bit

Autor:
Bryan Ahumada Ibarra


